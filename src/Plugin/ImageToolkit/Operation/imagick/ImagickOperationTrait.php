<?php

namespace Drupal\imagick\Plugin\ImageToolkit\Operation\imagick;

/**
 * Imagick Operation Trait.
 */
trait ImagickOperationTrait {

  /**
   * {@inheritdoc}
   */
  protected function execute(array $arguments) {
    return $this->processFrames($arguments);
  }

  /**
   * Process image frames for GIFs.
   *
   * @param array $arguments
   *   Arguments.
   *
   * @return bool
   *   Success or not.
   */
  protected function processFrames(array $arguments = []) {
    /** @var Imagick $resource */
    $resource = $this->getToolkit()->getResource();

    // If preferred format is set, use it as prefix for writeImage
    // If not this will throw a ImagickException exception.
    try {
      $image_format = $resource->getImageFormat();
    }
    catch (\ImagickException $e) {
    }

    $success = TRUE;
    if (isset($image_format) && in_array($image_format, ['GIF', 'WEBP'])) {
      // Get each frame in the GIF.
      $resource = $resource->coalesceImages();
      do {
        // @todo investigate phpstan warning.
        // @phpstan-ignore-next-line
        if (!$this->process($resource, $arguments)) {
          $success = FALSE;
          break;
        }
      } while ($resource->nextImage());

      $resource->deconstructImages();
    }
    else {
      // @todo investigate phpstan warning.
      // @phpstan-ignore-next-line
      $success = $this->process($resource, $arguments);
    }

    // Set the processed resource.
    $this->getToolkit()->setResource($resource);

    return $success;
  }

}
