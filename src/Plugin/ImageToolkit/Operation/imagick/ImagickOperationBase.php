<?php

namespace Drupal\imagick\Plugin\ImageToolkit\Operation\imagick;

use Drupal\Component\Utility\DeprecationHelper;
use Drupal\Component\Utility\Image;
use Drupal\Core\ImageToolkit\ImageToolkitOperationBase;

/**
 * Imagick Operation Base.
 *
 * @package Drupal\imagick\Plugin\ImageToolkit\Operation\imagick
 */
abstract class ImagickOperationBase extends ImageToolkitOperationBase {

  use ImagickOperationTrait;

  /**
   * Helper function to return the offset in pixels from the anchor.
   *
   * @param string $anchor
   *   The anchor ('top', 'left', 'bottom', 'right', 'center').
   * @param int $current_size
   *   The current size, in pixels.
   * @param int $new_size
   *   The new size, in pixels.
   *
   * @return int|string
   *   The offset from the anchor, in pixels, or the anchor itself, if its value
   *   isn't one of the accepted values.
   */
  protected function filterKeyword(string $anchor, int $current_size, int $new_size) {
    return DeprecationHelper::backwardsCompatibleCall(
      currentVersion: \Drupal::VERSION,
      deprecatedVersion: '11.1',
      currentCallable: fn() => Image::getKeywordOffset($anchor, $current_size, $new_size),
      deprecatedCallable: fn() => image_filter_keyword($anchor, $current_size, $new_size),
    );
  }

}
