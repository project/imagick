<?php

namespace Drupal\imagick\Plugin\ImageToolkit\Operation\imagick;

/**
 * Defines imagick annotate operation.
 *
 * @ImageToolkitOperation(
 *   id = "imagick_annotate",
 *   toolkit = "imagick",
 *   operation = "annotate",
 *   label = @Translation("Annotate"),
 *   description = @Translation("Annotates an image resource")
 * )
 */
class Annotate extends ImagickOperationBase {

  /**
   * {@inheritdoc}
   */
  protected function arguments() {
    return [
      'text_fieldset' => [
        'description' => 'Text settings.',
      ],
      'position_fieldset' => [
        'description' => 'Position settings.',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function process(\Imagick $resource, array $arguments) {
    $text = $arguments['text_fieldset'];
    $position = $arguments['position_fieldset'];

    $padding = [
      'x' => $position['padding_x'],
      'y' => $position['padding_y'],
    ];

    // Check if percent is used.
    $percent_x = explode('%', $padding['x']);
    if (count($percent_x) == 2) {
      $padding['x'] = $resource->getImageWidth() / 100 * reset($percent_x);
    }
    $percent_y = explode('%', $padding['y']);
    if (count($percent_y) == 2) {
      $padding['y'] = $resource->getImageHeight() / 100 * reset($percent_y);
    }

    // Create new transparent layer.
    $text_layer = new \Imagick();
    $text_layer->newImage(
      $resource->getImageWidth() - (2 * $padding['x']),
      $resource->getImageHeight() - (2 * $padding['y']),
      new \ImagickPixel('transparent')
    );

    // Font properties.
    $draw = new \ImagickDraw();
    $draw->setFont($text['font']);
    $draw->setFillColor($text['HEX']);
    $draw->setFontSize($text['size']);

    // Calculate text width and height.
    $imagick = new \Imagick();

    [$lines, $lineHeight] = $this->imageImagickWordWrapAnnotation($imagick, $draw, $text['text'], $text_layer->getImageWidth());

    // Calculate position.
    $text_dimensions = $imagick->queryFontMetrics($draw, $text['text']);
    $text_height = count($lines) * $lineHeight;
    [$left, $top] = explode('-', $position['anchor']);

    $y = (int) $this->filterKeyword($top, $text_layer->getImageHeight(), $text_height);
    $y += ($text_dimensions['textHeight'] + $text_dimensions['descender']);

    foreach ($lines as $line) {
      $line_dimensions = $imagick->queryFontMetrics($draw, $line);

      $x = (int) $this->filterKeyword($left, $text_layer->getImageWidth(), $line_dimensions['textWidth']);
      $text_layer->annotateImage($draw, $x, $y, 0, $line);

      // Add lineheight for next line.
      $y += $lineHeight;
    }

    return $resource->compositeImage($text_layer, \Imagick::COMPOSITE_OVER, $padding['x'], $padding['y']);
  }

  /**
   * Helper function to wrap text when it is to long.
   *
   * @param \Imagick $image
   *   Image.
   * @param \ImagickDraw $draw
   *   Draw.
   * @param string $text
   *   Text.
   * @param int $maxWidth
   *   Max width.
   *
   * @return array
   *   Lines and line-height.
   */
  private function imageImagickWordWrapAnnotation($image, $draw, $text, $maxWidth) {
    $text = trim($text);

    $words = preg_split('%\s%', $text, -1, PREG_SPLIT_NO_EMPTY);
    $lines = [];
    $i = 0;
    $lineHeight = 0;

    while (count($words) > 0) {
      $metrics = $image->queryFontMetrics($draw, implode(' ', array_slice($words, 0, ++$i)));
      $lineHeight = max($metrics['textHeight'], $lineHeight);

      // Check if we have found the word that exceeds the line width.
      if ($metrics['textWidth'] > $maxWidth or count($words) < $i) {
        // Handle case where a single word is longer than the allowed line
        // width (just add this as a word on its own line?).
        if ($i == 1) {
          $i++;
        }

        $lines[] = implode(' ', array_slice($words, 0, --$i));
        $words = array_slice($words, $i);
        $i = 0;
      }
    }

    return [$lines, $lineHeight];
  }

}
