(function ($, Drupal) {
  // Adds the JS that binds the textarea with the farbtastic element.
  // Find each colorpicker placeholder:
  // initialize it,
  // then find the nearby textfield that is of type colorentry
  // and attach the colorpicker behavior to it.
  // This is so we can support more that one per page if necessary.
  Drupal.behaviors.colorpicker = {
    attach() {
      $('.colorpicker').each(function () {
        const linkedTarget = $('.colorentry', $(this).closest('.colorform'));
        $.farbtastic($(this), linkedTarget);
      });
    },
  };
})(jQuery, Drupal);
