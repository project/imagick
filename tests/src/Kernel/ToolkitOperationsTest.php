<?php

declare(strict_types=1);

namespace Drupal\Tests\imagick\Kernel;

use Drupal\imagick\Plugin\ImageToolkit\ImagickToolkit;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests for imagick toolkit operations.
 *
 * @group imagick
 */
class ToolkitOperationsTest extends KernelTestBase {

  use ToolkitSetupTrait;

  /**
   * Path to image.
   */
  protected string $imagePath;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'imagick',
    'system',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['system', 'imagick']);
    $this->imagePath = $this->getTestFilesDirectory() . '/test-image.png';
  }

  /**
   * Tests supported extensions.
   *
   * @param string $toolkit_id
   *   The id of the toolkit to set up.
   * @param string $toolkit_config
   *   The config object of the toolkit to set up.
   * @param array $toolkit_settings
   *   The settings of the toolkit to set up.
   *
   * @dataProvider providerToolkitConfiguration
   */
  public function testSupportedExtensions(string $toolkit_id, string $toolkit_config, array $toolkit_settings) {
    $this->setUpToolkit($toolkit_id, $toolkit_config, $toolkit_settings);
    $image = $this->imageFactory->get();
    /** @var \Drupal\imagick\Plugin\ImageToolkit\ImagickToolkit $toolkit */
    $toolkit = $image->getToolkit();
    $supported_extensions = $toolkit::getSupportedExtensions();
    $this->assertTrue(in_array('png', $supported_extensions));
    $this->assertTrue(in_array('jpg', $supported_extensions));
    $this->assertTrue(in_array('jpeg', $supported_extensions));
  }

  /**
   * Tests the Imagick toolkit.
   *
   * @param string $toolkit_id
   *   The id of the toolkit to set up.
   * @param string $toolkit_config
   *   The config object of the toolkit to set up.
   * @param array $toolkit_settings
   *   The settings of the toolkit to set up.
   *
   * @dataProvider providerToolkitConfiguration
   */
  public function testToolkit(string $toolkit_id, string $toolkit_config, array $toolkit_settings) {
    $this->setUpToolkit($toolkit_id, $toolkit_config, $toolkit_settings);

    // Create a test image.
    $image = $this->imageFactory->get($this->imagePath);
    /** @var \Drupal\imagemagick\Plugin\ImageToolkit\ImagemagickToolkit $toolkit */
    $toolkit = $image->getToolkit();

    // Test basic image information.
    $this->assertInstanceOf(ImagickToolkit::class, $toolkit);

    // Test image dimensions.
    $this->assertSame(100, $image->getWidth());
    $this->assertsame(100, $image->getHeight());
  }

  /**
   * Tests resize operation.
   *
   * @param string $toolkit_id
   *   The id of the toolkit to set up.
   * @param string $toolkit_config
   *   The config object of the toolkit to set up.
   * @param array $toolkit_settings
   *   The settings of the toolkit to set up.
   *
   * @dataProvider providerToolkitConfiguration
   */
  public function testResize(string $toolkit_id, string $toolkit_config, array $toolkit_settings) {
    $this->setUpToolkit($toolkit_id, $toolkit_config, $toolkit_settings);

    // Create a test image.
    $image = $this->imageFactory->get($this->imagePath);

    // Test resize operation.
    $this->assertTrue($image->resize(50, 50));
    $this->assertEquals(50, $image->getWidth());
    $this->assertEquals(50, $image->getHeight());
  }

  /**
   * Tests composite operation.
   *
   * @param string $toolkit_id
   *   The id of the toolkit to set up.
   * @param string $toolkit_config
   *   The config object of the toolkit to set up.
   * @param array $toolkit_settings
   *   The settings of the toolkit to set up.
   *
   * @dataProvider providerToolkitConfiguration
   */
  public function testComposite(string $toolkit_id, string $toolkit_config, array $toolkit_settings) {
    $this->setUpToolkit($toolkit_id, $toolkit_config, $toolkit_settings);

    // Create a test image.
    $image = $this->imageFactory->get($this->imagePath);

    // Test composite operation.
    $this->assertTrue($image->apply('composite', [
      'path' => $this->imagePath,
      'composite' => \imagick::COMPOSITE_DEFAULT,
      'x' => '50',
      'y' => '50',
      'channel' => [\imagick::CHANNEL_DEFAULT],
    ]));
  }

}
