<?php

declare(strict_types=1);

namespace Drupal\Tests\imagick\Functional;

use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Session\AccountInterface;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests Imagick subform and settings.
 *
 * @group imagick
 */
class ToolkitImagickFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['system', 'imagick'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The admin user.
   */
  protected AccountInterface $adminUser;

  /**
   * Provides a list of available modules.
   */
  protected ModuleExtensionList $moduleList;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->moduleList = \Drupal::service('extension.list.module');

    // Create an admin user.
    $this->adminUser = $this->drupalCreateUser([
      'administer site configuration',
    ]);
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Test Imagick subform and settings.
   */
  public function testFormAndSettings(): void {
    $admin_path = 'admin/config/media/image-toolkit';

    // Change the toolkit.
    \Drupal::configFactory()->getEditable('system.image')
      ->set('toolkit', 'imagick')
      ->save();

    // Test form is accepting wrong binaries path while setting toolkit to GD.
    $this->drupalGet($admin_path);
    $this->assertSession()->fieldValueEquals('image_toolkit', 'imagick');
    $edit = [
      'image_toolkit' => 'gd',
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->fieldValueEquals('image_toolkit', 'gd');

    // Change the toolkit via form.
    $this->drupalGet($admin_path);
    $this->assertSession()->fieldValueEquals('image_toolkit', 'gd');
    $edit = [
      'image_toolkit' => 'imagick',
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->fieldValueEquals('image_toolkit', 'imagick');

    $edit = [
      'imagick[jpeg][jpeg_quality]' => 91,
      'imagick[jpeg][optimize]' => FALSE,
      'imagick[resize_filter]' => 8,
      'imagick[strip_metadata]' => FALSE,
    ];
    $expected_values = [
      'jpeg_quality' => 91,
      'optimize' => FALSE,
      'resize_filter' => 8,
      'strip_metadata' => FALSE,
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->pageTextContains('The configuration options have been saved.');

    foreach ($expected_values as $field => $expected_value) {
      $actual_value = $this->config('imagick.config')->get($field);
      $this->assertEquals($expected_value, $actual_value);
    }
  }

}
